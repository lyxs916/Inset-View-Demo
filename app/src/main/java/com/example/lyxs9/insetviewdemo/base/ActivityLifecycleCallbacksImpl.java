package com.example.lyxs9.insetviewdemo.base;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.example.lyxs9.insetviewdemo.utils.WindowDensityUtil;

import java.util.Stack;

public class ActivityLifecycleCallbacksImpl implements Application.ActivityLifecycleCallbacks {
    
    public static Stack<Activity> store = new Stack<>();
    
    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        //设置适配
        WindowDensityUtil.setCustomDensity(activity, activity.getApplication());
    }
    
    @Override
    public void onActivityStarted(Activity activity) {
    
    }
    
    @Override
    public void onActivityResumed(Activity activity) {
    
    }
    
    @Override
    public void onActivityPaused(Activity activity) {
    
    }
    
    @Override
    public void onActivityStopped(Activity activity) {
    
    }
    
    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    
    }
    
    @Override
    public void onActivityDestroyed(Activity activity) {
        store.remove(activity);
    }
}
