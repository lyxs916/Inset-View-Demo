package com.example.lyxs9.insetviewdemo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.example.lyxs9.insetviewdemo.view.KeyEventInterceptorEditText;

public class MainActivity2 extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        installViewFactory();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity2.this,AdaptiveActivity.class));
            }
        });
    }
    
    /** 使用自定义的LayoutInflater，便于动态替换自定义的view */
    private void installViewFactory() {
        LayoutInflaterCompat.setFactory2(LayoutInflater.from(this), new LayoutInflater.Factory2() {
            @Override
            public View onCreateView(String name, Context context, AttributeSet attrs) {
                return onCreateView(null, name, context, attrs);
            }
            
            @Override
            public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
                // 判断是否是EditText，如果是替换成KeyEventInterceptorEditText
                if (name.equals("EditText") || name.equals("android.support.v7.widget.AppCompatEditText")) {
                    return new KeyEventInterceptorEditText(context, attrs);
                }
                //appcompat 创建view代码
                AppCompatDelegate delegate = getDelegate();
                return delegate.createView(parent, name, context, attrs);
            }
        });
    }
}
