package com.example.lyxs9.insetviewdemo;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

import com.example.lyxs9.insetviewdemo.utils.WindowDensityUtil;

/**
 * 屏幕的适配测试
 */
public class AdaptiveActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //测试1
//        WindowDensityUtil.setCustomDensity(this, getApplication());
        //测试1
//        setCustomDensity(this, getApplication());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adaptive);
    }
    
    /**
     * 屏幕的适配，这个方法有bug
     */
    public static void setCustomDensity(@NonNull Activity activity, @NonNull Application application) {
        final DisplayMetrics appDisplayMetrics = application.getResources().getDisplayMetrics();
        final float targetDensity = appDisplayMetrics.widthPixels / 360;
        final int targetDensityDip = (int) (160 * targetDensity);
        
        appDisplayMetrics.density = appDisplayMetrics.scaledDensity = targetDensity;
        appDisplayMetrics.densityDpi = targetDensityDip;
        
        final DisplayMetrics activityDisplayMetrics = activity.getResources().getDisplayMetrics();
        activityDisplayMetrics.density = activityDisplayMetrics.scaledDensity = targetDensity;
        activityDisplayMetrics.densityDpi = targetDensityDip;
    }
    
}
