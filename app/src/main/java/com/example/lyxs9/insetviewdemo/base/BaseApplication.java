package com.example.lyxs9.insetviewdemo.base;

import android.app.Application;

public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //注册监听器
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacksImpl());
    }
}
