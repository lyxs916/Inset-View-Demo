package com.example.lyxs9.insetviewdemo.view;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;

/**
 * 说明：自定义的EditText
 * 一束光线：1050189980 2018/7/27
 */
public class KeyEventInterceptorEditText extends AppCompatEditText {
    
    public KeyEventInterceptorEditText(Context context) {
        super(context);
        setListener();
    }
    
    public KeyEventInterceptorEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setListener();
    }
    
    public KeyEventInterceptorEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setListener();
    }
    
    private void setListener() {
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //打印文本的变化
                Log.i("test", s.toString());
            }
            
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
    
    @Override
    public boolean dispatchKeyEventPreIme(KeyEvent event) {
        super.dispatchKeyEventPreIme(event);
        return true;
    }
}
